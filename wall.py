import vk
import re
import csv

service_token = 'YOUR_SERVICE_TOKEN'
api_version = 5.92
domain_address = 'chronometraz'

def get_posts_by_period(domain:str, start_date:str='', end_date:str=''):
    '''
        Get posts ids from given public by domain for period. Date format "YYYY.MM.DD"
        Uses service token, returns list of posts ids.
    '''
    session = vk.Session()
    api = vk.API(session, v=api_version)
    result = api.wall.get(access_token=service_token, domain=domain, count=100)
    posts = result['items']
    ids = []
    for post in posts:
        if (post['date'] >= date_to_unix(start_date)) and (post['date'] <= date_to_unix(end_date)):
            ids.append(post['id'])
    return ids

def get_text_by_postid(post_id:int, public_id:int=162534548):
    '''
        Get post text by post id. Returns list of dict: user_id, comment_text.
    '''
    session = vk.Session()
    api = vk.API(session, v=api_version)
    result = api.wall.getComments(access_token=service_token, owner_id=-public_id, post_id=post_id)
    if result:
        comments = result['items']
        texts = []
        for comment in comments:
            dict = {}
            dict['from_id'] = comment['from_id']
            dict['text'] = comment['text']
            texts.append(dict)
        return texts

def text_time_calculator(text:str):
    '''
        Calculates activities from text. Returns list of dictionaries.
    '''
    def hours_str_to_int(time_str: str):
        '''
            Converts hours in string to int format
        '''
        time_pattern = r'(?=\d{1,2}\sч)\d{1,2}'
        hours = re.findall(time_pattern, time_str)
        if hours:
            time_int = int(hours[0]) * 60
            return time_int
        else:
            return 0

    def minutes_str_to_int(time_str: str):
        '''
            Converts minutes in string to int format
        '''
        time_pattern = r'(?=\d{1,2}\sмин)\d{1,2}'
        minutes = re.findall(time_pattern, time_str)
        if minutes:
            time_int = int(minutes[0])
            return time_int
        else:
            return 0

    def line_to_dict(line):
        '''
            Converts line to dictionary for later use
        '''
        single_activity = r'(1 кат|2 кат|3 кат|Отдых|Дорога|Личное|Общение)\s\((\d{1,2}\sч\s?)?(\d{1,2}\sмин)?\)\:\s([а-яА-Я].*?)\.'
        multiple_activity = r'\n[а-яё]\)\s(.+?)\s[—|-]\s(\d{1,2}\sч\s?)?(\d{1,2}\sмин)?'
        category_name = r'(1 кат|2 кат|3 кат|Отдых|Дорога|Личное|Общение)'
        single = re.findall(single_activity, line)
        multiple = re.findall(multiple_activity, line)
        if single:
            single_dict = {}
            single_dict['category'] = single[0][0]
            single_dict['name'] = single[0][3]
            single_dict['minutes'] = minutes_str_to_int(single[0][2]) + hours_str_to_int(single[0][1])
            return single_dict
        elif multiple:
            category = re.findall(category_name, line)[0]
            multiple_list = []
            for activity in multiple:
                multiple_dict = {}
                multiple_dict['category'] = category
                multiple_dict['name'] = activity[0]
                multiple_dict['minutes'] = minutes_str_to_int(activity[2]) + hours_str_to_int(activity[1])
                multiple_list.append(multiple_dict)
            return multiple_list
        else:
            return {}

    if text.startswith(('1 кат', '2 кат', '3 кат', 'Дорога', 'Общение', 'Личное', 'Отдых')):
        lines = re.findall(r'(?:(?:1 кат|2 кат|3 кат|Дорога|Общение|Личное|Отдых).*?\.\s\n)|(?:Брутто:.*\.)', text, re.DOTALL)
        entries = []
        for line in lines:
            entry = line_to_dict(line)
            if entry:
                if isinstance(entry, list):
                    for item in entry:
                        entries.append(item)
                else:
                    entries.append(entry)
            else:
                entries.append({})

        return entries
    else:
        return [{}]

def unix_to_date(unix_date:int):
    '''
        Converts unix date to human format "YYYY.MM.DD". Time is local. Returns string.
    '''
    from datetime import datetime
    date_object = datetime.fromtimestamp(unix_date)
    return date_object.strftime('%Y.%m.%d')

def date_to_unix(date:str):
    '''
        Converts string with date "YYYY.MM.DD" into unix timestamp. Time is local.
        Returns int
    '''
    from datetime import datetime
    date_object = datetime.strptime(date, '%Y.%m.%d')
    return int(datetime.timestamp(date_object))

def activities_for_period(from_id:int, start_date:str, end_date:str):
    '''
        Make a list of activities for given period.
    '''
    def make_csv(file, data):
        FILENAME = file

        with open(FILENAME, "w", newline='', encoding='utf-8-sig') as file:
            columns = ["category", "name", "minutes"]
            writer = csv.DictWriter(file, fieldnames=columns)
            writer.writeheader()

            # запись нескольких строк
            writer.writerows(data)

    ids = get_posts_by_period('chronometraz', start_date, end_date)
    texts = []
    for id in ids:
        text = get_text_by_postid(id)
        if text:
            texts.append(text[0]['text'])

    raw_activities = []
    for text in texts:
        raw_activities += text_time_calculator(text)
    file_name = str(from_id) + '-' + start_date + '-' + end_date + '.csv'
    make_csv(file_name, raw_activities)
    return print('Done', file_name)